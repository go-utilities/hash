[![Go Reference](https://pkg.go.dev/badge/gitlab.com/go-utilities/file.svg)](https://pkg.go.dev/gitlab.com/go-utilities/hash)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-utilities/file)](https://goreportcard.com/report/gitlab.com/go-utilities/hash)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/go-utilities/file)](https://api.reuse.software/info/gitlab.com/go-utilities/hash)

# Go Utilities: hash

Go package with utilities that support working with hashes, extending the functionality of standard packages.
