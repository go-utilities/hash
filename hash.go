package hash

import (
	"crypto/md5"
	"fmt"
	"hash/fnv"
	"io"
	"os"
)

// HashUint64 calculates the FNV hash of the input as uint64
func HashUint64(format string, a ...interface{}) uint64 {
	h := fnv.New64a()
	fmt.Fprintf(h, format, a...)
	return uint64(h.Sum64())
}

// MD5 calculates the md5 hash of a file
func MD5(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}
